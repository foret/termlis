## Consonant mutations 

Initial consonant mutations are a particularity of breton shared with other languages in the celtic family. 

- The files in this directory give an overview as a diagram (with examples on nouns). 

     - See for example https://arbres.iker.cnrs.fr/index.php?title=Les_mutations_consonantiques for details. 

- See the TERMLIS/Breton/Verbs directory and displeger.bzh  site for details on verb mutations

