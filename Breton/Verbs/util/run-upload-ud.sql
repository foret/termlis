/*
-- https://universaldependencies.org/format.html

    ID: Word index, integer starting at 1 for each new sentence; may be a range for multiword tokens; may be a decimal number for empty nodes (decimal numbers can be lower than 1 but must be greater than 0).
    FORM: Word form or punctuation symbol.
    LEMMA: Lemma or stem of word form.
    UPOS: Universal part-of-speech tag.
    XPOS: Optional language-specific (or treebank-specific) part-of-speech / morphological tag; underscore if not available.
    FEATS: List of morphological features from the universal feature inventory or from a defined language-specific extension; underscore if not available.
    HEAD: Head of the current word, which is either a value of ID or zero (0).
    DEPREL: Universal dependency relation to the HEAD (root iff HEAD = 0) or a defined language-specific subtype of one.
    DEPS: Enhanced dependency graph in the form of a list of head-deprel pairs.
    MISC: Any other annotation.
*/

--  V2.14  --

/*	echo "Integrating CONLLU in PostgreSQL (june 24, V1, then V2.14)" 

set fileUD=br_keb-ud-test-2.14.conllu 

	echo " ***  Output summary ***"
	echo "use $fileUD-line.csv  for lines per word  in the treebank, in two columns, the first column is the line number in the original treebank file" 
	echo "use $fileUD-line-desc.csv  for lines starting with # in the treebank, in two columns, the first column is the line number in the original treebank file" 

lancer psql 
*/

--  V2.14  --
--
-- Import du treebank, partie un mot par ligne 

 drop table if exists br_keb_ud ;
 create table br_keb_ud (line  integer, WID varchar(50), FORM varchar(50), LEMMA varchar(50), UPOS varchar(50), XPOS varchar(50), FEATS varchar(250), HEAD varchar(50), DEPREL varchar(50), DPS varchar(50), MISC varchar(250)) ;
 
 \copy br_keb_ud(line, WID, FORM, LEMMA, UPOS, XPOS, FEATS, HEAD, DEPREL, DPS, MISC)  from ../br_keb-ud-test-2.14.conllu-line.csv delimiter  E'\t'  csv  ESCAPE '\' 
 
 
 --  V2.14  --
--
-- Import du treebank, partie lignes commençant par # 
-- attention la colonne sent etait nommée desc (mais pas OK en postgresql), et la taille 250 était insuffisante dans cette version

 drop table if exists br_keb_sent ;
 create table br_keb_sent (line  integer, sent varchar(300)) ;
 
 \copy br_keb_sent(line, sent)  from ../br_keb-ud-test-2.14.conllu-line-desc.csv delimiter  E'\t'  csv  ESCAPE '\' 


--  vues utiles pour l'export (vueVerbSent* refaits 18 juin24)

drop view if exists br_keb_ud_row  ; 
create view br_keb_ud_row as 
select br_keb_ud.*, ROW_NUMBER() OVER (partition by LEMMA, UPOS order by LEMMA, UPOS, line) as row_asc
from br_keb_ud 
order by LEMMA, UPOS, line
; 

--
-- vues utiles facultatives (26 juin24), si on veut lier à la phrase en format UD 
--    doc : https://www.postgresqltutorial.com/postgresql-window-function/postgresql-row_number/
--

/* drop view if exists br_keb_ud_num  ; 
create view br_keb_ud_num as 
select *, ROW_NUMBER () OVER (order by line)  as row_num
from br_keb_ud_row 
where row_asc=1
order by line
; 
*/

drop view if exists br_keb_sentnum  ; 
create view br_keb_sentnum as 
select *, ROW_NUMBER () OVER (order by line)  as row_sentnum
from   br_keb_sent  as S1
where S1.SENT like '%#%text =%'  
order by line
; 



--  V2.14  --
--
-- Export pour liens dans fichier html des verbes 
--   on ajoute un rang de lemma, pour avoir des id uniques, 
--       partie gardée et améliorée en vue des variantes d'affichage par css
-- selon la version : from  br_keb_sentnum  as S1  à simplifier en : from br_keb_sent  as S1
-- dans la version UD 2.14 les text en fr sont après les textes en eng, adapter : S2.line=S1.line+2  au lieu de  S2.line=S1.line+1 

-- version Fr, si V 2.14
-- revision 7 juillet 2024 pour mieux controler les occ. à remplacer, 
--    ex: SELECT REGEXP_REPLACE('# text = N int,ket aet war-raok .',' int( |,|;|.|:|!)', ' _\1'); -- pas |?
--   remplacment simple,  à rendre plus robuste en utilisant la position (plus robuste dans l'arbre)
-- , concat(replace(S1.SENT,W.FORM,concat('<span class="solution" title="',W.FORM,'">','_','</span>','<span class="w" title="',W.FORM,'">',W.FORM,'</span>'))

drop view if exists vueVerbSentExport ;
create view vueVerbSentExport
as 
select  W.LEMMA, W.row_asc, 
concat('<p class="sent">'
, '<a id="', W.LEMMA,'-', W.row_asc,'" '
       , 'href="https://displeger.bzh/fr/verb/', W.LEMMA, '"', ' >'
, W.LEMMA
, '</a> '
, '<span data-form="', W.FORM, '" ' ,'data-root="[r]" >', W.FORM, '</span> '
, '<span class="line" data-sentnum="',S1.row_sentnum,'"> (line ', S1.line,', sent ',S1.row_sentnum,') </span>' 
, concat(regexp_replace(S1.SENT,concat(' ',W.FORM,'( |,|;|.|:|!)' ),concat('<span class="solution" title="',W.FORM,'">',' _\1','</span>','<span class="w" title="',W.FORM,'">',W.FORM,'</span>'))
, '<span class="sentFr"> ',S2.SENT,'</span>' 
 )
, '<p>') as SENT 
from br_keb_ud_row as W, br_keb_sentnum  as S1, br_keb_sent  as S2
where  UPOS='VERB' and DEPREL='root' and 
S1.SENT like '%#%text =%' 
and S1.line < W.line 
and S1.line = (select max (S.line )
from br_keb_sent  as S
where S.line < W.line  and S.SENT like '%#%text =%' ) 
and S2.line=S1.line+2 
  union
select  W.LEMMA, W.row_asc, 
concat('<p class="sent">'
, '<a id="', W.LEMMA,'-', W.row_asc,'" '
       , 'href="https://displeger.bzh/fr/verb/', W.LEMMA, '"', ' >'
, W.LEMMA
, '</a> ' 
, '<span data-form="', W.FORM, '" ' ,'data-root="[-]" >', W.FORM, '</span> '
, '<span class="line" data-sentnum="',S1.row_sentnum,'"> (line ', S1.line,', sent ',S1.row_sentnum,') </span>' 
, concat(regexp_replace(S1.SENT,concat(' ',W.FORM,'( |,|;|.|:|!)' ),concat('<span class="solution" title="',W.FORM,'">',' _\1','</span>','<span class="w" title="',W.FORM,'">',W.FORM,'</span>'))
, '<span class="sentFr"> ',S2.SENT,'</span>' 
)
, '<p>') as SENT 
from br_keb_ud_row as W, br_keb_sentnum  as S1, br_keb_sent  as S2
where  UPOS='VERB' and DEPREL<>'root' and 
S1.SENT like '%#%text =%' 
and S1.line < W.line 
and S1.line = (select max (S.line )
from br_keb_sent  as S
where S.line < W.line  and S.SENT like '%#%text =%' ) 
and S2.line=S1.line+2 
;

\copy (select SENT from vueVerbSentExport
order by LEMMA, row_asc
)
to ../vueVerbSent-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS ''
; 

-- version Eng, si V 2.14

drop view if exists vueVerbSentEngExport ;
create view vueVerbSentEngExport
as 
select  W.LEMMA, W.row_asc, 
concat('<p class="sent">'
, '<a id="', W.LEMMA,'-', W.row_asc,'" '
       , 'href="https://displeger.bzh/fr/verb/', W.LEMMA, '"', ' >'
, W.LEMMA
, '</a> '
, '<span data-form="', W.FORM, '" ' ,'data-root="[r]" >', W.FORM, '</span> '
, '<span class="line" data-sentnum="',S1.row_sentnum,'"> (line ', S1.line,', sent ',S1.row_sentnum,') </span>' 
, concat(regexp_replace(S1.SENT,concat(' ',W.FORM,'( |,|;|.|:|!)' ),concat('<span class="solution" title="',W.FORM,'">',' _\1','</span>','<span class="w" title="',W.FORM,'">',W.FORM,'</span>'))
, '<span class="sentEng"> ',S2.SENT,'</span>' 
 )
, '<p>') as SENT 
from br_keb_ud_row as W, br_keb_sentnum  as S1, br_keb_sent  as S2
where  UPOS='VERB' and DEPREL='root' and 
S1.SENT like '%#%text =%' 
and S1.line < W.line 
and S1.line = (select max (S.line )
from br_keb_sent  as S
where S.line < W.line  and S.SENT like '%#%text =%' ) 
and S2.line=S1.line+1 
union
select  W.LEMMA, W.row_asc, 
concat('<p class="sent">'
, '<a id="', W.LEMMA,'-', W.row_asc,'" '
       , 'href="https://displeger.bzh/fr/verb/', W.LEMMA, '"', ' >'
, W.LEMMA
, '</a> ' 
, '<span data-form="', W.FORM, '" ' ,'data-root="[-]" >', W.FORM, '</span> '
, '<span class="line" data-sentnum="',S1.row_sentnum,'"> (line ', S1.line,', sent ',S1.row_sentnum,') </span>' 
, concat(regexp_replace(S1.SENT,concat(' ',W.FORM,'( |,|;|.|:|!)' ),concat('<span class="solution" title="',W.FORM,'">',' _\1','</span>','<span class="w" title="',W.FORM,'">',W.FORM,'</span>'))
, '<span class="sentEng"> ',S2.SENT,'</span>' 
)
, '<p>') as SENT 
from br_keb_ud_row as W, br_keb_sentnum  as S1, br_keb_sent  as S2
where  UPOS='VERB' and DEPREL<>'root' and 
S1.SENT like '%#%text =%' 
and S1.line < W.line 
and S1.line = (select max (S.line )
from br_keb_sent  as S
where S.line < W.line  and S.SENT like '%#%text =%' ) 
and S2.line=S1.line+1 
;

\copy (select SENT from vueVerbSentEngExport
order by LEMMA, row_asc
)
to ../vueVerbSentEng-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS ''
;

