drop view if exists vueVerbSentRDF ;
create view vueVerbSentRDF
as 
select  concat('<dvb:',W.LEMMA,'>rel:inSentNum ', S1.row_sentnum, ' ; rel:inUDLine ', S1.line,
                 ' ; rel:inSent ', '<dvb:sentNum', S1.row_sentnum,'> ', '.') as one, 
        concat('<dvb:sentNum', S1.row_sentnum,'> a <dvb:Sent>', ' ; rel:text "', S1.SENT,'" ', ' ; rel:fr "', S2.SENT,'" .') as two
from br_keb_ud_row as W, br_keb_sentnum  as S1, br_keb_sent  as S2
where  UPOS='VERB' and 
S1.SENT like '%#%text =%' 
and S1.line < W.line 
and S1.line = (select max (S.line )
from br_keb_sent  as S
where S.line < W.line  and S.SENT like '%#%text =%' ) 
and S2.line=S1.line+2 
;

\copy (select concat(one, two) from vueVerbSentRDF
)
to vueVerbSentRDF-inc.ttl csv DELIMITER E'\t' QUOTE E'\b' NULL AS ''

-- cat vueAll.ttl vueVerbSentRDF-inc.ttl > vueVerbSentRDF.ttl
