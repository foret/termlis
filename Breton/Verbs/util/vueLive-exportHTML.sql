-- --------------------------------------------------------

--
-- Concerne des  tags des données du conjugueur <a href=https://displeger.bzh/br>DVB</a> 
--       variante SQL utilisée : PostgreSQL (commande linux psql)
--         [AF (foret@irisa.fr), 11/6/24]
--

--
-- Exemple d'export pour les tags par niveau européen (A1, etc.) 
--

--
-- Vue support
--

drop view if exists vue_live ; 
create view vue_live as 
select  L.id as idL, V.id as idV, L.infinitive, L.base, L.category, T.translation, T.language_code, tag.id as idT, tag.code, tag.category_id   
from public.verblocalization as L, public.verb as V, verbtranslation as T, verb_tag as VT, tag 
where  
L.verb_id=V.id and T.verb_id = V.id and VT.verb_id=V.id 
and VT.tag_id=tag.id
and tag.code like '%live%'
 order by tag.code, L.infinitive, V.id ;

--
-- Export csv, avec ";" en séparateur et les noms de colonne 
--

\copy (select * from vue_live)
 to ../vueLive.csv csv HEADER DELIMITER E';' QUOTE E'\b' NULL AS '';

-- ou bien, en changeant l'ordre des colonnes, avec "," en séparateur et les noms de colonne 

\copy (select  infinitive, idV,  base, category, translation, language_code, idT, code, category_id    from vue_live)
 to ../vueLive.csv csv HEADER DELIMITER E','  NULL AS '';

--
-- Export html 
--

/* -- ajouter des lignes de balisage html en début (avec css) et en  fin dans ../vueLive-css.html 
*/

\copy (select concat('<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',substring(code,14,2)
  ,'" class="',code
  ,'" > '
,infinitive
,' </a>') as item from vue_live 
where language_code='fr'
order by code, infinitive 
 )
 to ../vueLive.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

--
-- Export RDF turtle (format standard décrit ici : https://www.w3.org/TR/turtle/)
--  test possible dans https://www.ldf.fi/service/rdf-grapher (on peut copier/coller un extrait )
--

/* -- ajouter ces déclarations en début de fichier .ttl (turtle) :

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@base <https://displeger.bzh/fr/verb/> .
@prefix rel: <https://displeger.bzh/rel/> .
@prefix dvb: <https://dvb/> .

<dvb:A1> a <dvb:LiveTag> .
<dvb:A2> a <dvb:LiveTag> .
<dvb:B1> a <dvb:LiveTag> .
<dvb:B2> a <dvb:LiveTag> .
<dvb:C1> a <dvb:LiveTag> .
<dvb:C2> a <dvb:LiveTag> .

<dvb:LiveTag> rdfs:subClassOf <dvb:Tag> .
*/

/* -- note : infinitive may have blanks, we take the word before, for the ttl format 
*/
\copy (select concat('<',split_part(infinitive,' ',1),'>'
                , 'a <dvb:Verb> ; '
				,' rel:id ','"',idV,'" ; '
			    ,' rel:fr ','"',translation,'" ; '
                ,' rel:category ','"',category,'" ; '
                ,' rel:base ','<dvb:',base,'>; '
                ,' rel:tag ','<dvb:',substring(code,14,2),'> . '
                ) as item from vue_live 
where language_code='fr'
order by code, infinitive 
 )
 to ../vueLive.ttl csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';
