# Intermediate and memo files, developped for a verb-centered workflow

## To export a html version (without sentence) from PostgreSQL
    see vueAll-exportHTML.sql 

## To upload a UD treebank in PostgreSQL (and to include sentences in HTML export)  
    see run-upload.* files (and run-concat*.sh)

## To rewrite a universal dependencies (UD) treebank
    see rewrite*.grs files and run-grew-ud.sh

We provide this rewriting system to hide verb forms in the corpus: 
   rewrite-hide-verb-form.grs

that can used with: 
   run-grew-ud.sh
   
- method1: loaded the .grs file on web.grew  and apply it to a loaded corpus
- method2: run a shell command similar to run-grew-ud.sh (tcsh version), 
then load the output corpus in a UD treebank viewer 
  https://urd2.let.rug.nl/~kleiweg/conllu/ 
    and search for example for a verb infinitive 
  or     https://web.grew.fr/
  other viewers are proposed at https://universaldependencies.org/

 ## Alternative output
    see files *Live* 
for a preliminary version limited to the "Live tag" (level, such as A1, A2)
