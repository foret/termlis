	echo "Integrating CONLLU in PostgreSQL (june 24, V1, then V2.14)" 

    echo "-- Phase une ligne par mot --"
	echo "tcsh (adapter si bash) et indiquer le fichier source"
set fileUD=br_keb-ud-test-2.14.conllu 

	echo "ajouter le numero de ligne en première colonne"
cat -n $fileUD > $fileUD-line
echo "$fileUD-line created"

	echo "obtenir les lignes par mot des phrases"
	echo "# extrait des mots (pas les lignes vides, ni celles de description avec # à gauche)"

sed -n 's/^\(.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*$\)/\1/p' $fileUD-line > $fileUD-line.csv
	echo "# ajouter des quote (après avoir remplacé \" par &quote; -  line 1858...)"
	echo "# sed -e 's/\"/\&quote;/g' $fileUDbr-line.csv | grep 1858"
sed -i -e 's/"/\&quote;/g' $fileUD-line.csv
echo "$fileUD-line.csv created"

sed -e 's/\t/"\t"/g' $fileUD-line.csv | sed -e 's/^/"/g' |  sed -e 's/$/"/g' > $fileUD-line-quote.csv
echo "$fileUD-line-quote.csv created"

    echo "-- Phase les lignes de commentaire des phrases --"
	echo "on peut vérifier que le # est spécifique à ces lignes (sans tab)"
	echo "grep '#' $fileUD | more"
grep '#' $fileUD | sed -n 's/\t/\t/p'     
	echo "(vide)" 

grep '#' $fileUD-line > $fileUD-line-desc.csv
echo "$fileUD-line-desc.csv created"

	echo " ***  Output summary ***"
	echo "use $fileUD-line.csv  for lines per word  in the treebank, in two columns, the first column is the line number in the original treebank file" 
	echo "use $fileUD-line-desc.csv  for lines starting with # in the treebank, in two columns, the first column is the line number in the original treebank file" 
