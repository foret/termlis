
cat vueSubclassbis-debut.html > vueSubclassbis-css.html 

echo '<details data-subclass="TypeArgs"><summary>Args tags (transitif, etc.)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueArgsbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeChange"><summary>Change tags (lettres interchangeables)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueChangebis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeConstruct"><summary>Construct tags (à partir de préfixe, etc.)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueConstructbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeDomain"><summary>Domain tags (domaines)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueDomainbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeEnds"><summary>Ends tag (terminaison de l'infinitif)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueEndsbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeLevel"><summary>Level tags (niveaux)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueLevelbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeLink"><summary>Link tag (liens)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueLinkbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeNbSyllabs"><summary>NbSyllabs tags (nombre de syllabes de l'infinitif)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueNbSyllabsbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeSubstring"><summary>Substring tags (sous-chaines difficiles)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueSubstringbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeSynset"><summary>Synset tags (nombre de synonymes fr)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueSynsetbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 
echo '<details data-subclass="TypeOther"><summary>Other tags (autres descriptifs)</summary><details class="none"><summary></summary>' >> vueSubclassbis-css.html  ; cat Subclass/vueOtherbis-inc.html >> vueSubclassbis-css.html  ; echo '</details></details>' >> vueSubclassbis-css.html 

  echo "# pour version avec Phrases + Fr du corpus UD en breton br_keb_ud"
cp vueSubclassbis-css.html vueSubclassSent-debut.html 

  echo "# fin version sans Phrases du corpus UD en breton br_keb_ud"
cat vueSubclassbis-fin.html >> vueSubclassbis-css.html 

  echo "# fin version avec Phrases du corpus UD en breton br_keb_ud"
cp vueSubclassSent-debut.html   vueSubclassSent.html
echo '<h2> Phrases du corpus UD en breton br_keb_ud </h2><details><summary>Liste de phrases selon leur verbe </summary>' >> vueSubclassSent.html 
echo '<span class="it">L'"'"'infinitif à gauche est lié au site DVB, il est suivi de la phrase du corpus où ce  verbe est masqué, sa forme exacte dans la phrase apparait en passant sur le souligné.</span>' >> vueSubclassSent.html
cat vueVerbSent-inc.html >> vueSubclassSent.html 
echo '</details>' >> vueSubclassSent.html 
cat vueSubclassbis-fin.html >> vueSubclassSent.html 

  echo "# pour version avec Phrases + Eng du corpus UD en breton br_keb_ud"
cp vueSubclassSent-debut.html   vueSubclassSentEng.html
echo '<h2> Sentences from Breton UD corpus br_keb_ud </h2><details><summary>Sentence list according to their verb</summary>' >> vueSubclassSentEng.html 
echo '<span class="it">The infinitive on the left is linked to the DVB site, it is followed by the corpus sentence where the verb is hidden, its exact form appears on hover over _.</span>' >> vueSubclassSentEng.html
cat vueVerbSentEng-inc.html >> vueSubclassSentEng.html 
echo '</details>' >> vueSubclassSentEng.html 
cat vueSubclassbis-fin.html >> vueSubclassSentEng.html 


# DONE (numero de phrase) TODO - ajouter lien vers l'affichage de l'arbre UD ou une transformation de cet arbre 
# echo '<span class="it">Le symbole <a href="annotatrix.html#1">&#x1F333;</a> est lié à la page qui affiche la phrase en arbre UD.'  >> vueSubclassSent.html
