-- --------------------------------------------------------

--
-- Concerne des  tags des données du conjugueur <a href=https://displeger.bzh/br>DVB</a> 
--       variante SQL utilisée : PostgreSQL (commande linux psql)
--         [AF (foret@irisa.fr), 11/6/24]
--

--
-- Exemple d'export pour les tags par niveau européen (A1, etc.) et les autres
--

--
-- Vue support
--

drop view if exists vue_all ; 
create view vue_all as 
select  L.id as idL, V.id as idV, L.infinitive, L.base, L.category, T.translation, T.language_code, tag.id as idT, tag.code, tag.category_id   
from public.verblocalization as L, public.verb as V, verbtranslation as T, verb_tag as VT, tag 
where  
L.verb_id=V.id and T.verb_id = V.id and VT.verb_id=V.id 
and VT.tag_id=tag.id
-- and tag.code like '%live%'
 order by tag.code, L.infinitive, V.id ;

-- cette vue y ajoute le numéro de ligne du résultat, en redémarrant  à 1 pour chaque code
--   voir doc : https://www.postgresqltutorial.com/postgresql-window-function/postgresql-row_number/


drop view if exists vue_all_row  ; 
create view vue_all_row as 
select ROW_NUMBER() OVER (partition by code order by code, infinitive, idV) as row_asc
      ,  vue_all.* 
     -- , tagFr.label as labelFr, tagFr.description as descriptionFr
from vue_all 
      -- , tag_translation as tagFr 
        where  language_code='fr'
-- and tagFr.tag_id=vue_all.idT  and tagFr.language_code='fr'
order by code, infinitive, idV ;

/* test possible : select code, infinitive, row_asc from vue_all_row ; */

--
-- Export csv, avec ";" en séparateur et les noms de colonne 
--

\copy (select * from vue_all)
 to ../vueAll.csv csv HEADER DELIMITER E';' QUOTE E'\b' NULL AS '';

-- ou bien, en changeant l'ordre des colonnes, avec "," en séparateur et les noms de colonne 

\copy (select  infinitive, idV,  base, category, translation, language_code, idT, code, category_id    from vue_all)
 to ../vueAll.csv csv HEADER DELIMITER E','  NULL AS '';

--
-- Export html, avec remplacement des espaces et des quotes dans le code 
--

/* -- ajouter ensuite des lignes de balisage html en début (avec css) et en  fin dans ../vueAll-css.html 
      ligne de commande possible (linux) : 
              cat vueAll-debut.html vueAll-inc.html vueAll-fin.html > vueAll-css.html
              cat vueAllbis-debut.html vueAllbis-inc.html vueAllbis-fin.html > vueAllbis-css.html
              
*/


\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"> <summary>',code,'</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>') as item from vue_all_row
where language_code='fr'
order by code, row_asc 
 )
 to ../vueAll-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';


--
-- ou bien avec des tags en français  :from  tag_translation as tagFr 
--   avec :    tagFr.label as labelFr, tagFr.description as descriptionFr
--
 
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>') as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
order by code, row_asc 
 )
 to ../vueAll-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';


--
-- ou encore plus avec un deuixème lien interne (t.q. vers des phrases de corpus au format UD conll)  
-- usage : cat vueAllbis-debut.html vueAllbis-inc.html vueAllbis-fin.html > vueAllbis-css.html
--

\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
order by code, row_asc 
 )
 to ../vueAllbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';



-- Exports semblables html par sousclasse de tags [ajout 25 juin 2024]
-- adapter le where : and code in (select code from TypeSynset)
-- usage : cat vueAllbis-debut.html vueAllbis-inc.html vueAllbis-fin.html > vueAllbis-css.html

-- TypeSynset
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeSynset)
order by code, row_asc 
 )
 to ../vueSynsetbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeLevel
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeLevel)
order by code, row_asc 
 )
 to ../vueLevelbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeDomain
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeDomain)
order by code, row_asc 
 )
 to ../vueDomainbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeLink
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeLink)
order by code, row_asc 
 )
 to ../vueLinkbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeSubstring
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeSubstring)
order by code, row_asc 
 )
 to ../vueSubstringbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeNbSyllabs
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeNbSyllabs)
order by code, row_asc 
 )
 to ../vueNbSyllabsbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeEnds
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeEnds)
order by code, row_asc 
 )
 to ../vueEndsbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeChange
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeChange)
order by code, row_asc 
 )
 to ../vueChangebis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeConstruct
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeConstruct)
order by code, row_asc 
 )
 to ../vueConstructbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeArgs
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeArgs)
order by code, row_asc 
 )
 to ../vueArgsbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- TypeOther
\copy (select concat(
 regexp_replace(concat('</details> <details class="',cast (row_asc=1 as varchar),'"',' title="',tagFr.description,'"> <summary>',code,' (',tagFr.label, ')','</summary>')
        ,'.*="false".*'
        ,''),
 '<a href="https://displeger.bzh/fr/verb/',infinitive
 ,'" data-id="',idL
 ,'" title="',translation
 ,'" data-init="',substring(infinitive,1,1)
 ,'" data-categ="',category
 ,'" data-base="',base
 ,'" data-idV="',idV
  ,'" data-tag="',replace(replace(code,' ','_'),'"','&quote;') 
  ,'" data-row="',row_asc
  ,'" > '
,infinitive
,' </a>' 
,'<a class="bis" href="#',infinitive,'-1" > ', ' #',' </a>' ) as item from vue_all_row 
             , tag_translation as tagFr 
where vue_all_row.language_code='fr'
              and tagFr.tag_id=vue_all_row.idT  and tagFr.language_code='fr'
              and code in (select code from TypeOther)
order by code, row_asc 
 )
 to ../vueOtherbis-inc.html csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

-- Export RDF turtle (format standard décrit ici : https://www.w3.org/TR/turtle/)
--  test possible dans https://www.ldf.fi/service/rdf-grapher (on peut copier/coller un extrait )
--

/* -- ajouter ensuite ces déclarations en début de fichier .ttl (turtle) :
 (ou par la commande :  cp vueAll.html vueAll-.ttl ; cat vueAll-debut.html vueAll-.ttl > vueAll.ttl)
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@base <https://displeger.bzh/fr/verb/> .
@prefix rel: <https://displeger.bzh/rel/> .
@prefix dvb: <https://dvb/> .

<dvb:A1> a <dvb:LiveTag> .
<dvb:A2> a <dvb:LiveTag> .
<dvb:B1> a <dvb:LiveTag> .
<dvb:B2> a <dvb:LiveTag> .
<dvb:C1> a <dvb:LiveTag> .
<dvb:C2> a <dvb:LiveTag> .

<dvb:LiveTag> rdfs:subClassOf <dvb:Tag> .
*/

/* -- note : infinitive may have blanks, we take the word before, for the ttl format 
*/
\copy (select concat('<',split_part(infinitive,' ',1),'>'
                , 'a <dvb:Verb> ; '
				,' rel:idV ','"',idV,'" ; '
			    ,' rel:fr ','"',replace(translation,'"','_quote_'),'" ; '
                ,' rel:category ','"',category,'" ; '
                ,' rel:base ','<dvb:',base,'>; '
                ,' rel:tag ','<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_') ,'> . '
                ) as item from vue_all 
where language_code='fr'
order by code, infinitive 
 )
 to ../vueAll.ttl csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';
