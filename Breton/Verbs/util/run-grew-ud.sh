	echo "to hide the  language verb form in the treebank"
	echo "to adapt: replace br_keb-ud-test-2.14 by your  treebank name"	
	echo "choose the set of grew rewrite rules: replace hide-verb-form.grs with  hide-verb.grs  or etc."
grew transform -grs rewrite-hide-verb-form.grs -i br_keb-ud-test-2.14.conllu -o br_keb-ud-test-2.14-hide-verb-form.conllu
	echo "rewrite rule applied, br_keb-ud-test-2.14-hide-verb.conllu created"
    
    echo "run next line to hide the sentence text"
sed -i -e 's/^\( *# text *= *\).*$/\1 (hidden)/g' br_keb-ud-test-2.14-hide-verb-form.conllu 
	echo "original sentences erased from meta-data in br_keb-ud-test-2.14-hide-verb.conllu (otherwise it appears in the viewer)"

