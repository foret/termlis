# Files for a verb-centered workflow

     These files are part of an experimental workflow built on these two recent resources for Breton: 
    - https://displeger.bzh/en (a database version and a website version), 
      this links to the Breton verb conjugator (DVB  for "displeger verboù brezhonek"), provided by An Drouizig (https://drouizig.org/)
    - https://universaldependencies.org/treebanks/br_keb(a treebank of Breton structured sentences)
      this links to the UD Breton-KEB, a treebank of Breton that has been manually annotated according to the Universal Dependencies guidelines.

    The aim of this development is twofold: 
      - as a use case for the FCA paradigm, within the SmartFCA ANR research project (smartfca.org) ; 
      - as a human-centered prototype, to help a language learner, while providing access to valuable resources. 
    Moreover, Breton raises  challenges  as an endangered and low-resourced language. 



## HTML output 
     see *.css* *.html)

for a test with verbs from DVB + sentences from the UD corpus 

    - use (open in a navigator, having copied the .css) 
            vueSubclassSentEng.html () 
        or  vueSubclassSent.html (similar with french translations)

  these HTML files are prepared with two sections : 
  - a hierachy of tags  (open a tag to view the verbs having this tag) 
  - a list of prepared sentences (a verb selected in the first section   links to related sentences that hide the exact verb)

## csv output
    (see *.csv) more explanations to come 

## ttl/RDF output
    (see *.ttl) more explanations to come 


## QCM output
     (see examples directory for a prototype version and a preliminary test)
