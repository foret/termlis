## This repertory contains files examples, that apply to breton linguistic data 

### Treebank extract (source data)
    see *.conllu (one sentence in *sent1.conllu)

### Work under development  includes :

- test files for QCM generation from linguistic data
     
      see QCM*.txt

to be loaded on text2quiz (described here: https://www.tice-education.fr/tous-les-articles-er-ressources/articles-internet/1590-text2quiz-un-editeur-de-quiz-libre-et-gratuit)

- test files for RDF browsing/searching (over linguistic data)

      see *.sparql 
    

