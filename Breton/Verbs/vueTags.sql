-- --------------------------------------------------------

--
-- Vue et Classification des tags des données du conjugueur <a href=https://displeger.bzh/br>DVB</a> 
--         selon des propriétés de leur code, label (fr) et description (fr) 
--         variante SQL utilisée : PostgreSQL (commande linux psql)
--         [AF (foret@irisa.fr), 11/6/24] 
--         revised 25/6/24, adding  subclass column in views, does not affect the exports in .ttl
--		   the full subclass list is : Synset (for translation information), Level, Domain, Link, Substring, NbSyllabs, Ends (for verb ending), Change (for variations), Construct, Args (for transitive, etc.), Other
--         revised 25/6/24, adding the TypefrAll view as the union all these Type* views 
--  --------------------------------------------------------


--
-- Vue principale 
--

drop view if exists vueVerbsWithTags ;
create view vueVerbsWithTags as 
select  V.id, L.infinitive, L.base, L.category, T.translation, T.language_code, tag.id as idT, tag.code, tag.category_id
  , tagFr.label, tagFr.description 
from public.verblocalization as L, public.verb as V, verbtranslation as T, verb_tag as VT, tag, tag_translation as tagFr 
where  
L.verb_id=V.id and T.verb_id = V.id and VT.verb_id=V.id 
and VT.tag_id=tag.id
and tagFr.tag_id=tag.id 
and tagFr.language_code='fr'
 order by tag.code, L.infinitive, V.id ;

--
-- Stats vue principale
--

select count(*) from vueVerbsWithTags ; 
  -- 55125 rows 


select count(distinct id) from verb ; 
   -- 11663 rows

/* -- autre requête possible, mais lignes assez nombreuses  
select count(code) as nb, code, label
from  vueVerbsWithTags 
group by code, label, description
having count(code) >100 
order by nb desc ;
*/ 

--
-- Vues secondaires (pour classes de tags - organisation de facettes)  
--

/* -- un export possible pour une vue d'ensemble des tags 
\copy (
select distinct code, label, description
from  vueVerbsWithTags 
group by code, label, description
)
 to ../vueTags.txt 
 csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';
*/


drop view if exists TypeSynset ; 
create view TypeSynset as (
select  code, label, description, 'Synset' as subclass
from  vueVerbsWithTags 
where (label like '%concept%pour%traduire%' or label like '%mot%pour%traduire%' )
group by code, label, description 
) ; -- 26 rows

drop view if exists TypeLevel ; 
create view TypeLevel as (
select  code, label, description, 'Level' as subclass
from  vueVerbsWithTags 
where label like '%niveau%' 
group by code, label, description 
) ; -- 3 rows

drop view if exists TypeDomain ; 
create view TypeDomain as (
select  code, label, description, 'Domain' as subclass
from  vueVerbsWithTags 
where description like '%utilis%' or description like '%domain%' 
group by code, label, description 
) ; -- 144 rows

drop view if exists TypeLink ; 
create view TypeLink as (
select  code, label, description, 'Link' as subclass
from  vueVerbsWithTags 
where description like '%en lien%' or  description like '%un lien%'
group by code, label, description 
) ; -- 12 rows

drop view if exists TypeSubstring  ; 
create view TypeSubstring as (
select  code, label, description, 'Substring' as subclass
from  vueVerbsWithTags 
where label like '%qui%contient%' 
group by code, label, description 
) ; -- 8 rows

drop view if exists TypeNbSyllabs ; 
create view TypeNbSyllabs as (
select  code, label, description, 'NbSyllabs' as subclass
from  vueVerbsWithTags 
where description like '%qui%contient%syllab%' 
group by code, label, description 
) ; -- 6 rows

drop view if exists TypeEnds ; 
create view TypeEnds as (
select  code, label, description, 'Ends' as subclass
from  vueVerbsWithTags 
where label like '%finissant%par%' 
group by code, label, description 
) ; -- 9 rows

drop view if exists TypeChange ; 
create view TypeChange as (
select  code, label, description, 'Change' as subclass
from  vueVerbsWithTags 
where label like '%interchangeable%avec%' 
group by code, label, description 
) ; -- 9 rows

drop view if exists TypeConstruct ; 
create view TypeConstruct as (
select  code, label, description, 'Construct' as subclass
from  vueVerbsWithTags 
where label like '%construit%partir%' 
group by code, label, description 
) ; -- 11 rows

drop view if exists TypeArgs ; 
create view TypeArgs as (
select  code, label, description, 'Args' as subclass
from  vueVerbsWithTags 
where label like '%transiti%' 
group by code, label, description 
) ; -- 5 rows

--
-- Les lignes qui ne sont pas répertoriées dans ces vues de classes de tags
-- 
drop view if exists TypeOther ; 
create view TypeOther as (
select distinct code, label, description, 'Other' as subclass  
from vueVerbsWithTags 
where not ((label like '%concept%pour%traduire%' or label like '%mot%pour%traduire%' ) 
          or
           label like '%niveau%' 
           or 
           (description like '%utilis%' or description like '%domain%' )
           or
            description like '%en lien%' or  description like '%un lien%'
           or label like '%qui%contient%' 
           or description like '%qui%contient%syllab%' 
           or label like '%finissant%par%' 
           or label like '%interchangeable%avec%' 
           or label like '%construit%partir%' 
           or label like '%transiti%'
            ) 
group by code, label, description 
)
; -- 44 rows

drop view if exists TypefrAll; 
create view TypefrAll(code, label, description,subclass) as
select * from TypeSynset
union select * from TypeLevel
union select * from TypeDomain
union select * from TypeLink
union select * from TypeSubstring 
union select * from TypeNbSyllabs
union select * from TypeEnds 
union select * from TypeChange
union select * from TypeConstruct
union select * from TypeArgs
union select * from TypeOther
order by subclass, code, label, description 
-- --------------------------------------------------------
-- Exports
--  --------------------------------------------------------

--
-- Export RDF des Vues secondaires (pour classes de tags - organisation de facettes)  
--  modèle : <dvb:A1> a <dvb:LiveTag> . <dvb:LiveTag> rdfs:subClassOf <dvb:Tag> .
-- fichier à inclure après une section de déclarations RDF Turtle
--  exemple :       cat vueAll-debut.ttl vueTags-inc.ttl > vueTags.ttl
--                  cat vueAll.ttl vueTags-inc.ttl > vueTagsAll.ttl
--  usage :  ./fuseki-server --file vueTags.ttl /dvbTags
--           ./fuseki-server --file vueTagsAll.ttl /dvbTagsAll

--
-- Export RDF des tags, Variante 1, avec juste les labels en breton
--

\copy (
select '<dvb:SynsetTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:SynsetTag> .') as item
from TypeSynset  
 union 
select '<dvb:LevelTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
 select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:LevelTag> .') as item 
from TypeLevel 
 union
select '<dvb:DomainTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:DomainTag> .') as item
from TypeDomain  
 union
select '<dvb:LinkTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:LinkTag> .') as item
from TypeLink  
 union
select '<dvb:SubstringTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:SubstringTag> .') as item
from TypeSubstring  
 union
select '<dvb:NbSyllabsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:NbSyllabsTag> .') as item
from TypeNbSyllabs  
 union
select '<dvb:EndsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:EndsTag> .') as item
from TypeEnds  
 union
select '<dvb:ChangeTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ChangeTag> .') as item
from TypeChange  
 union
select '<dvb:ConstructTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ConstructTag> .') as item
from TypeConstruct  
 union
select '<dvb:ArgsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ArgsTag> .') as item
from TypeArgs  
 union
select '<dvb:OtherTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:OtherTag> .') as item
from TypeOther  
)
 to ../vueTags-inc.ttl csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';

--
-- ou bien : Export RDF des tags, Variante 2, qui ajoute les labels en français
-- 

\copy (
select '<dvb:SynsetTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:SynsetTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeSynset 
 union 
select '<dvb:LevelTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
 select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:LevelTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeLevel 
 union
select '<dvb:DomainTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:DomainTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeDomain  
 union
select '<dvb:LinkTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:LinkTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeLink  
 union
select '<dvb:SubstringTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:SubstringTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeSubstring  
 union
select '<dvb:NbSyllabsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:NbSyllabsTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeNbSyllabs  
 union
select '<dvb:EndsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:EndsTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeEnds  
 union
select '<dvb:ChangeTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ChangeTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeChange  
 union
select '<dvb:ConstructTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ConstructTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeConstruct  
 union
select '<dvb:ArgsTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:ArgsTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeArgs  
 union
select '<dvb:OtherTag> rdfs:subClassOf <dvb:Tag> .' as item
 union
select concat('<dvb:',replace(replace(replace(code,'>','_sup_')  ,' ','_'),'"','_quote_'),'> a <dvb:OtherTag> ; '  
              , ' rel:fr ','"',replace(label,'"','_quote_'),'" . ') as item
from TypeOther  
)
 to ../vueTags-inc.ttl csv DELIMITER E'\t' QUOTE E'\b' NULL AS '';
