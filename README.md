# TermLis

This site is an extension of the initial TermLis software at  http://www.irisa.fr/LIS/softwares/TermLis/ as an application of Logical Information Systems to terminological resources. 

Work under development  includes : 
  - a new section on breton data 
  - data preparation for https://www.smartfca.org/



## License
This is an open source project (CC0).

## Project status
The project is under  development.
